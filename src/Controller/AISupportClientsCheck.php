<?php

namespace Drupal\ai_support_bot\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * AI Support Bot clear class.
 *
 * @AISupportBotSearchCheck
 * Defines AISupportBotSearchCheck Controller class.
 * Get a response code from any URL using Guzzle in Drupal 8!
 *
 * Usage:
 * In the head of your document:
 *
 * use Drupal\custom_guzzle_request\Http\CustomGuzzleHttp;
 *
 * In the area you want to return the result, using any URL for $url:
 *
 * $check = new CustomGuzzleHttp();
 * $response = $check->performRequest($url);
 **/
class AISupportClientsCheck extends ControllerBase
{
    /**
     * To check if this variable exist in @getKeyword()
     *
     * @var SearchKey
     */
    protected $config;
    protected $baseUrl;
    protected $url;
    protected $loginUrl;
    protected $logOutUrl;
    protected $clientFactory;

    /**
     * Constructs a new HelloForm object.
     */
    public function __construct()
    {
        $this->baseUrl = 'https://ryan.ddkits.com';
        $this->config = \Drupal::config('ai_support_bot.settings');
        $this->connection = \Drupal::database();
        $this->currentUser = \Drupal::currentUser();
        $this->url = $this->baseUrl . '/api/v1/ai_support_bot/clients?_format=json';
        $this->loginUrl = $this->baseUrl . '/user/login?_format=json';
        $this->logOutUrl = $this->baseUrl . '/user/logout?_format=json';
        $this->clientFactory = \Drupal::service('http_client_factory');
    }

    /**
     *
     */
    public function ai_analytics_send_questions($question = '', $qid = 0, $answer = '', $hosts = '')
    {
        $username = $this->config->get('share_analytic_ddkits_username');
        $pass = $this->config->get('share_analytic_ddkits_password');
        $auth = 'Basic ' . base64_encode($username . ':' . $pass);
        $url = $this->url;
        $question = str_replace(' ', '_', $question);
        $answer = str_replace(' ', '_', $answer);
        $host = ($hosts != '')?:$_SERVER['HTTP_HOST'];
        $curl = curl_init();
        // If token retrieved then continue.
        curl_setopt_array($curl, array(
              CURLOPT_URL => $url,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => "{\n\t\"question\": \"".$question."\",\n\t\"answer\": \"".$answer."\",\n\t\"qid\": ".$qid.",\n\t\"hosts\": ".$host."\n}",
              CURLOPT_HTTPHEADER => array(
                "Accept: application/json",
                "Authorization: " . $auth,
                "Cache-Control: no-cache",
                "Content-Type: application/json",
              ),
            ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if (!$err) {
            \Drupal::logger('ai_support_bot')->notice('Client Sent result:' . print_r($response, true) . ' no errors');
        } else {
            \Drupal::logger('ai_support_bot')->notice('Client Sent result:' . print_r($response, true) . ' error: ' .  $err);
        }
    }
}
