<?php

namespace Drupal\ai_support_bot\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * AI Support Bot clear class.
 *
 * @AIQAnswers
 * Defines AIQAnswers Controller class.
 */
class AIQAnswers extends ControllerBase
{
    protected $connection;
    protected $config;
    protected $bot;

    /**
     * Constructs a new object.
     */
    public function __construct()
    {
        $this->bot = '<b>Ryan </b>';
        $this->config = \Drupal::config('ai_support_bot.settings');
        $this->connection = \Drupal::database();
    }

    /**
     * Get question.
     */
    public function getQuestionAnswer($question)
    {
        $question = trim($question);
        if (strtolower($question) == 'no' || strtolower($question) == 'nope' || strtolower($question) == 'not') {
            return 'I see, sorry for that. Let\'s try again, How can I help you?';
        }
        $noNeedWords = [
      'the', 'on', 'if', 'that', 'is', 'a', 'at', 'are',
      'for', 'about', 'looking', 'what', 'but', 'you', 'not', 'this',
      'i\'m', 'am', 'i am', 'iam',
    ];

        $questionsExpectation = [
      'how', 'who', 'what', 'when', 'do', 'does', 'how', 'where', 'could',
      'should', 'i', ',', '?', '.', '!', 'cannot', 'can', 'does', 'do', ' ', '-', '_'
    ];
        $randomAnswer = [
      ', does this help?',
      ', hope this could help?',
      ', hope this is helpful?',
      ', hope this answered your question?',
      ', is this what you are looking for?',
      ', did this answer your question?',
    ];
        $randomAnswer1 = [
      'hmm, This is what I found ',
      'This is what I found ',
      'Here\'s what I found ',
      'Maybe this can help ',
    ];
        $randomNoAnswer = [
      ' Sorry couldn\'t understand your question "' . $question . '", can you be more specific please!',
      ' Your question "' . $question . '", is not clear!',
      ' I couldn\'t find answer to "' . $question . '"',
      ' Sometimes as a bot I cannot understand all questions, for example "' . $question . '", try another something else',
      ' I don\'t have an answer to "' . $question . '"',
      ' Life is hard sometimes, nothing related to "' . $question . '"',
      ' I am not positive that I understand what you are asking "' . $question . '". Please try rewording your question.',
    ];
        $q1 = strtolower($question);
        $qArr = explode(' ', $q1);
        $answers = [];
        $questions = '';
        $result = [];
        $answer = 'no-answer';
        $query = db_select('ai_support_bot', 'v');
        $query->fields('v');
        $betterAnswer = [];
        $results = $query->execute()->fetchAll();
        $qid = 0;
        foreach ($results as $key => $value) {
            // Prepare the answers for the multiple logics.
            $keyword = strtolower($value->words);
            // If the question 100% equal the keywords.
            if (strtolower($value->words) == strtolower($question)) {
                return $value->answers;
            }
            $expectedAnswer = [
        'answer' => $value->answers,
        'qid' => $value->qid,
        'keyword' => $keyword,
      ];
            $betterAnswer[$key]['count'] = 0;
            // Test if question has the words in full.
            if (strpos($keyword, $q1) !== false && !in_array($keyword, $questionsExpectation) && !in_array($keyword, $noNeedWords)) {
                if (!in_array($expectedAnswer, $answers)) {
                    $answers[$key] = $expectedAnswer;
                }
                $betterAnswer[$key]['count']++;
            }
            // Test if question has the words in full.
            if (strpos($keyword, '<or>') !== false) {
                $newWords = $this->multiexplode(['<or>', ' <or>', ' <or> ', '<or> '], $value->words);
                foreach ($newWords as $newkey => $newvalue) {
                    if (in_array($newvalue, $qArr) && !in_array($newvalue, $questionsExpectation) && !in_array($newvalue, $noNeedWords)) {
                        if (!in_array($expectedAnswer, $answers)) {
                            $answers[$key] = $expectedAnswer;
                        }
                        $betterAnswer[$key]['count']++;
                    }
                    if (strpos($newvalue, $q1) !== false && !in_array($newvalue, $questionsExpectation) && !in_array($newvalue, $noNeedWords)) {
                        if (!in_array($expectedAnswer, $answers)) {
                            $answers[$key] = $expectedAnswer;
                        }
                        $betterAnswer[$key]['count']++;
                    }
                    if (strpos($q1, $newvalue) !== false && !in_array($newvalue, $questionsExpectation) && !in_array($newvalue, $noNeedWords)) {
                        if (!in_array($expectedAnswer, $answers)) {
                            $answers[$key] = $expectedAnswer;
                        }
                        $betterAnswer[$key]['count']++;
                    }
                    // Test if question multiple words has the words in full.
                    if (strpos($newvalue, ' ') !== false) {
                        $newvalueArr = explode(' ', $newvalue);
                        foreach ($newvalueArr as $newkey1 => $newvalue1) {
                            $check = str_replace(' ', '', $newvalue1) . ' ';
                            if (strpos($q1, $check) !== false && !in_array($newvalue1, $questionsExpectation) && !in_array($newvalue1, $noNeedWords)) {
                                if (!in_array($expectedAnswer, $answers)) {
                                    $answers[$key] = $expectedAnswer;
                                }
                                $betterAnswer[$key]['count']++;
                            }
                        }
                    }
                }
            }
            // Test if question multiple words has the words in full.
            if (strpos($keyword, ' ') !== false) {
                $newvalueArr = explode(' ', $keyword);
                foreach ($newvalueArr as $newkey => $newvalueIs) {
                    $check = str_replace(' ', '', $newvalueIs) . ' ';
                    if (strpos($q1, $check) !== false && !in_array($newvalueIs, $questionsExpectation) && !in_array($newvalueIs, $noNeedWords)) {
                        if (!in_array($expectedAnswer, $answers)) {
                            $answers[$key] = $expectedAnswer;
                        }
                        $betterAnswer[$key]['count']++;
                    }
                }
            }
            // Test if question contains the word.
            if (in_array($keyword, $qArr)) {
                if (!in_array($expectedAnswer, $answers) && !in_array($keyword, $questionsExpectation) && !in_array($keyword, $noNeedWords)) {
                    $answers[$key] = $expectedAnswer;
                }
                $betterAnswer[$key]['count']++;
            }
        }
        // Filter answers again, top in list is the highest priority.
        if (count($answers) > 0) {
            $x = 0;
            $vaKey = 0;
            foreach ($betterAnswer as $key => $value) {
                $y = $value;
                if ($y >= $x) {
                    $vaKey = $key;
                    $x = $y;
                }
            }
            $answer = $answers[$vaKey]['answer'];
            // $answer = '<pre>' .print_r($answers, true) . '<pre>' . print_r($betterAnswer, true);
            $qid = $answers[$vaKey]['qid'];
        }

        // Search contents by title.
        $answersResult = [];
        $questionContent = [];
        // If ($answer == 'no-answer') {.
        $query = db_select('node_field_data', 'v');
        $query->fields('v');
        $results = $query->execute()->fetchAll();
        $rm1 = array_rand($randomAnswer1);
        $i = 0;
        foreach ($results as $key => $value) {
            // Test if question contains the word.
            $title = explode(' ', $value->title);
            foreach ($title as $key1 => $tvalue) {
                if (!in_array(strtolower($tvalue), $noNeedWords) && !in_array(strtolower($tvalue), $questionsExpectation)) {
                    if (in_array(strtolower($tvalue), $qArr)) {
                        if (count($answersResult) < 5 && !in_array('<a target="_blank" href="/node/' . $value->nid . '">' . $value->title . '</a>', $answersResult)) {
                            $answersResult[] = '<a target="_blank" href="/node/' . $value->nid . '">' . $value->title . '</a>';
                            if (!in_array($tvalue, $questionContent)) {
                                $questionContent[] = $tvalue;
                            }
                        } elseif (count($answersResult) >= 5 && !in_array('<a target="_blank" href="/search/node?keys=' . $question . '"> Or try our search to look for more results </a>', $answersResult)) {
                            $answersResult[] = '<a target="_blank" href="/search/node?keys=' . $question . '"> Or try our search to look for more results </a>';
                        }
                    }
                }
            }
        }
        if (count($answersResult) > 0) {
            $rm = array_rand($randomAnswer);
            if ($answer == 'no-answer' || count($answers) == 0) {
                $answer = $randomAnswer1[$rm1];
            } else {
                $answer .= '. Also, ' . $randomAnswer1[$rm1] . ' about ' . implode(',', $questionContent);
            }

            foreach ($answersResult as $key => $value) {
                $answer .= '<br> - ' . $value . '.';
            }
            $answer .= '<br>' . $randomAnswer[$rm];
        }
        // }
        if ($answer == 'no-answer') {
            $rm = array_rand($randomNoAnswer);
            $answer = $randomNoAnswer[$rm1];
            $this->storeNewQuestion($question, 0, $answer);
            return $answer;
        }

        if ($answer == 'no-answer') {
            $answer = $this->validateSearchBasics($question);
        }
        $this->storeNewQuestion($question, $qid, $answer);

        // If not found.
        return $answer;
    }

    /**
     * Store question.
     */
    private function storeNewQuestion($question = '', $qid = 0, $answer = '')
    {
        if ($this->config->get('share_analytic_ddkits') == true) {
            $user = \Drupal::currentUser();
            // Empty the table.
            $values = [];
            $query = db_select('ai_support_bot_new_questions', 'v');
            $query->condition('v.question', $question);
            $query->condition('v.answer', $answer);
            $query->fields('v');
            $results = $query->execute()->fetchAll();
            if (!empty($results)) {
                foreach ($results as $key => $value) {
                    // Test if question contains the word.
                    if (strtolower($value->question) == strtolower($question)) {
                        $query = \Drupal::database()->update('ai_support_bot_new_questions');
                        $query->fields([
            'times_repeated' => ((int) $value->times_repeated + 1),
            'created' => \Drupal::time()->getRequestTime(),
          ]);
                        $query->condition('qid', $value->qid);
                        $query->condition('question', strtolower($question));
                        break;
                    }
                }
            } else {
                $values[] = [
        'question' => $question,
        'answer' => $answer,
        'qid' => $qid,
        'times_repeated' => 1,
        'created' => \Drupal::time()->getRequestTime(),
        'status' => 1,
        'uid' => ($user->id()) ?: '0',
      ];
                $query = $this->connection->insert('ai_support_bot_new_questions')->fields(['question', 'answer', 'qid', 'times_repeated', 'created', 'uid', 'status']);
                foreach ($values as $record) {
                    $query->values($record);
                }
            }
            $results = $query->execute();
            $qid = 0;
            // IF analytics is on.
            $send = new AISupportClientsCheck();
            try {
                $host = \Drupal::request()->getHost();
                $send->ai_analytics_send_questions($question, $qid, $answer, $host);
            } catch (\Throwable $th) {
                // \Drupal::logger('ai_support_bot')->error('error in AI analytics <pre>' . $th . '</pre>');
            }
        }
        // End of send.
        return;
    }

    /**
     *
     */
    public function multiexplode($delimiters, $string)
    {
        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);
        return $launch;
    }

    /**
     *
     */
    public function validateSearchBasics($SearchCheck)
    {
        $Content = '';
        $standardQuestions = [
      "hi" => "Hello, My name is '. $this->bot . ' how can I help you today?",
      "bye" => "It was nice talking to you. Have a good one!",
      "yes" => "Great, happy I could help, any other question?",
      "no" => "Alright, I'm sorry, maybe you can ask me something else, or rephrase your question please",
    ];
        if (in_array(strtolower($SearchCheck), $standardQuestions)) {
            foreach ($standardQuestions as $key => $value) {
                if (strtolower($SearchCheck) == $key) {
                    $Content = '<br>' . $this->bot . ' ' . $value;
                } else {
                    $Content = 'no-answer';
                }
            }
        }
        return $Content;
    }
}
