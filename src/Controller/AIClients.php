<?php

namespace Drupal\ai_support_bot\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * AI Support Bot clients class.
 *
 * @AIClients
 * Defines AISupportBotSearchCheck Controller class.
 */
class AIClients extends ControllerBase
{
    /**
     * To check if this variable exist in @getKeyword()
     *
     * @var SearchKey
     */
    protected $SearchKey;
    protected $currentUser;

    /**
     * Constructs a new HomeController object.
     */
    public function __construct()
    {
        $this->SearchKey = 'no';
        $this->currentUser = \Drupal::currentUser();
    }

    /**
     *
     */
    public function getNewQuestions()
    {
        $uid = $this->currentUser->id();
        $query = db_select('ai_support_bot_analytics', 'v');
        $query->fields('v');
        $query->condition('v.uid', $uid);
        $results = $query->execute()->fetchAll();
        $final = json_decode(json_encode($results), true);
        return $final;
    }

    public function content(AccountInterface $user, Request $request)
    {
        $uid = $this->currentUser->id();
        $headers=['id', 'qid', 'question', 'answer', 'times_repeated'];
        $header = array(
          // We make it sortable by name.
          array('data' => $this->t('id'), 'field' => 'id', 'sort' => 'desc'),
          array('data' => $this->t('Answer from your list'), 'field' => 'qid'),
          array('data' => $this->t('question'), 'field' => 'question'),
          array('data' => $this->t('answer'), 'field' => 'answer'),
          array('data' => $this->t('times_repeated'), 'field' => 'times_repeated', 'sort' => 'desc'),
          array('data' => $this->t('Website'), 'field' => 'website'),
        );

        $query = db_select('ai_support_bot_analytics', 'v');
        $query->fields('v', $headers);
        $query->condition('v.uid', $uid);
        // The actual action of sorting the rows is here.
        $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')
                          ->orderByHeader($header);
        // Limit the rows to 20 for each page.
        $pager = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender')
                          ->limit(20);
        $result = $pager->execute();

        // Populate the rows.
        $rows = array();
        foreach ($result as $row) {
            $rows[] = array('data' =>
                [
                  'ID' => $row->id,
                  'Question ID'=>$row->qid,
                  'Question' => $row->question,
                  'Answered by Ryan' => $row->answer,
                  'Repeated Times' => $row->times_repeated,
                  'Website' => $row->hosts,
                ]
          );
        }

        // The table description.
        $build = array(
        '#markup' => t('List of All Questions answered by Ryan Support')
      );

        // Generate the table.
        $build['config_table'] = array(
        '#theme' => 'table',
        '#header' => $header,
        '#rows' => $rows,
      );

        // Finally add the pager.
        $build['pager'] = array(
        '#type' => 'pager'
      );

        return $build;
    }

    public function ai_support_bot_user_login($account)
    {
        // We want to redirect user on login.
        $response = new RedirectResponse('/user/'.$account-id() . '/dashboard');
        $response->send();
        return;
    }
}
