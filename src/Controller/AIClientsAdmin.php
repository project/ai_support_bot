<?php

namespace Drupal\ai_support_bot\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * AI Support Bot clients class.
 *
 * @AIClientsAdmin
 * Defines AISupportBotSearchCheck Controller class.
 */
class AIClientsAdmin extends ControllerBase
{
    /**
     * To check if this variable exist in @getKeyword()
     *
     * @var SearchKey
     */
    protected $SearchKey;
    protected $config;
    protected $bot;

    /**
     * Constructs a new object.
     */
    public function __construct()
    {
        $this->bot = '<b>Ryan </b>';
        $this->config = \Drupal::config('ai_support_bot.settings');
        $this->SearchKey = 'no';
    }

    /**
     *
     */
    public function getNewQuestions()
    {
        $query = db_select('ai_support_bot_new_questions', 'v');
        $query->fields('v');
        $results = $query->execute()->fetchAll();
        $final = json_decode(json_encode($results), true);
        return $final;
    }

    public function content(AccountInterface $user, Request $request)
    {
        $headers=['id', 'qid', 'question', 'answer', 'times_repeated'];
        $header = array(
          // We make it sortable by name.
          array('data' => $this->t('id'), 'field' => 'id', 'sort' => 'desc'),
          array('data' => $this->t('Answer from your list'), 'field' => 'qid'),
          array('data' => $this->t('question'), 'field' => 'question'),
          array('data' => $this->t('answer'), 'field' => 'answer'),
          array('data' => $this->t('times_repeated'), 'field' => 'times_repeated', 'sort' => 'desc'),
        );

        $query = db_select('ai_support_bot_new_questions', 'v');
        $query->fields('v', $headers);
        // The actual action of sorting the rows is here.
        $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')
                          ->orderByHeader($header);
        // Limit the rows to 20 for each page.
        $pager = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender')
                          ->limit(20);
        $result = $pager->execute();

        // Populate the rows.
        $rows = array();
        foreach ($result as $row) {
            $rows[] = array('data' =>
                [
                  'ID' => $row->id,
                  'Question ID'=>$row->qid,
                  'Question' => $row->question,
                  'Answered by Ryan' => $row->answer,
                  'Repeated Times' => $row->times_repeated,
                ]
          );
        }

        // // Organize rows{}
        // $rows1 = array();
        // foreach ($rows as $key => $value) {
        //     # code...
        // }

        // The table description.
        $build = array(
        '#markup' => t('List of All Questions answered by Ryan Support')
      );

        // Generate the table.
        $build['config_table'] = array(
        '#theme' => 'table',
        '#header' => $header,
        '#rows' => $rows,
      );

        // Finally add the pager.
        $build['pager'] = array(
        '#type' => 'pager'
      );

        return $build;
    }
}
