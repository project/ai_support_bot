<?php

namespace Drupal\ai_support_bot\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * AI Support Bot clients class.
 *
 * @AItextToSpeech
 * Defines AItextToSpeech Controller class.
 */
class AItextToSpeech extends ControllerBase {

  /**
   *
   */
  public function text2Speech($answer) {
    $txt = rawurlencode(substr(strip_tags($answer), 0, 200));
    $file = "public://audio/" . md5($answer) . ".mp3";
    if (!file_exists($file)) {
      // The spaces in the sentence are replaced with the Plus symbol.
      $url = 'http://translate.google.com/translate_tts?ie=UTF-8&client=gtx&q=' . $txt . '&tl=en';
      $response = \Drupal::httpClient()->get($url);
      $data = (string) $response->getBody();
      // If the MP3 file exists, do not create a new request
      // if (!file_exists($file)) {.
      file_put_contents($file, $data);
    }
    $path = file_create_url($file);
    $player = '<audio style="display:none;" autoplay src="' . $path . '" type="audio/mp3" ></audio>';
    return $player;
  }

}
