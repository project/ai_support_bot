<?php

namespace Drupal\ai_support_bot\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'AI Support Need help Ask Ryan' block.
 *
 * @Block(
 *   id = "ai_support_bot_need_help_ask_block",
 *   admin_label = @Translation("AI Form Support Need help Ask Ryan block"),
 *   category = @Translation("DDKits")
 * )
 */
class AIFormNeedHelpBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('Drupal\ai_support_bot\Form\AIFormAskRyan');
    return $form;
  }

}
