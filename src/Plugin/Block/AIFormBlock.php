<?php

namespace Drupal\ai_support_bot\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'AI Form Support' block.
 *
 * @Block(
 *   id = "ai_support_bot_form_block",
 *   admin_label = @Translation("AI Form Support block"),
 *   category = @Translation("DDKits")
 * )
 */
class AIFormBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('Drupal\ai_support_bot\Form\AIForm');
    return $form;
  }

}
