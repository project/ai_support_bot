<?php

namespace Drupal\ai_support_bot\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\ai_support_bot\Controller\AIQAnswers;
use Drupal\ai_support_bot\Controller\AItextToSpeech;

/**
 *
 */
class AIFormAskRyan extends FormBase
{
    protected $connection;
    protected $mainConfig;
    protected $newChat;

    /**
     * Constructs a new HelloForm object.
     */
    public function __construct()
    {
        $this->bot = '<b>Ryan </b>';
        $this->mainConfig = \Drupal::config('ai_support_bot.settings');
        $this->connection = \Drupal::database();
        $this->newChat = true;
    }

    /**
     * Returns a unique string identifying the form.
     *
     * The returned ID should be a unique string that can be a valid PHP function
     * name, since it's used in hook implementation names such as
     * hook_form_FORM_ID_alter().
     *
     * @return string
     *   The unique string identifying the form.
     */
    public function getFormId()
    {
        return 'ai_support_bot_ask_ryan_form';
    }

    /**
     * Form constructor.
     *
     * @param array $form
     *   An associative array containing the structure of the form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The current state of the form.
     *
     * @return array
     *   The form structure.
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['ai_support_bot_ask_ryan'] = [
      '#type' => 'fieldset',
      '#weight' => 0,
      '#attributes' => [
        'hidden' => false,
        'style' => 'border-radius: 5px;display:none;position:fixed;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);right:5px;bottom:10px;width:30%;min-width:300px;height:500px;z-index:5;background:white;',
        'class' => ['form-control'],
      ],
    ];
        $form['ai_support_bot_ask_ryan']['ask_ryan_top_menu'] = [
      '#type' => 'container',
      '#weight' => -50,
      '#attributes' => [
        'action' => false,
        'hidden' => false,
        'style' => 'display:flex;height:50px;width:100%;z-index:5;background:white;',
        'class' => ['form-control'],
      ],
    ];
        // Links only.
        $form['show_ryan_ask'] = [
      '#type' => 'textfield',
      '#value' => 'Need help? ask Ryan',
      '#attributes' => [
        'hidden' => false,
        'style' => 'display:inline-block;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);position: fixed;right:5px;bottom:10px;width:180px;height:30px;z-index:10;background:white;cursor: pointer;
            color: black;
            padding: 10px;
            border-radius: 5px;
            transform: translateX(2px); ',
        'class' => [''],
      ],
    ];

        $form['ai_support_bot_ask_ryan']['ask_ryan_top_menu']['hide_ryan_ask'] = [
      '#type' => 'textfield',
      '#value' => 'X',
      '#attributes' => [
        'hidden' => false,
        'style' => 'display:inline-block;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);position: absolute;left:-20px;top:-20px;width:20px;height:30px;z-index:10;background:white;cursor: pointer;
            color: black;
            float:right;
            border-radius: 40px;
            transform: translateX(2px); ',
        'class' => [''],
      ],
    ];
        // Text to speech.
        if ($this->mainConfig->get('text_to_speech') == 1) {
            $form['ai_support_bot_ask_ryan']['ask_ryan_top_menu']['ask_ryan_block_menu'] = [
        '#type' => 'container',
        '#attributes' => [
          'hidden' => false,
          'style' => 'display:block;position:absolute;right: 0; top:0;',
        ],
      ];
            $form['ai_support_bot_ask_ryan']['ask_ryan_top_menu']['ask_ryan_block_menu']['ask_ryan_mute_text_to_speech'] = [
        '#type' => 'checkbox',
        '#title' => 'Mute',
        '#default_value' => true,
        '#attributes' => [
          'hidden' => false,
          'style' => 'float:right;position:relative; width:30px;margin: 0px;',
        ],
          // '#description' => t('Check this box to Mute me if you don\'t like to hear my voice.'),
      ];
        }
        // End links only.
        $form['ryan_ask_startup_message'] = [
      '#type' => 'hidden',
      '#value' => '<div class="ryan-questions" style="border:1px solid black;background:#f4f4f4;color:black;padding:10px;width:70%;"><b>' . $this->bot . '</b><br>' . $this->mainConfig->get('startup_support_message') . '</div>',
    ];
        $form['ai_support_bot_ask_ryan']['ask_ryan_answer_div'] = [
      '#type' => 'container',
      '#weight' => 0,
      '#attributes' => [
        'hidden' => false,
        'style' => 'display:block;overflow: auto;overflow-y: scroll;position:relative;width:100%;height:auto;max-height:400px;margin: 0px;',
        'class' => ['ask-ryan-search-valid-message-answers'],
      ],
    ];
        // Sending messages box.
        $form['ai_support_bot_ask_ryan']['ask_ryan_sending_msg_div'] = [
      '#type' => 'container',
      '#weight' => 0,
      '#attributes' => [
        'hidden' => false,
        'style' => 'display:flex;overflow: auto;overflow-y: scroll;position:absolute;bottom:0;right:0;width:100%;height:auto;max-height:200px;margin: 0px;',
      ],
    ];

        $form['ai_support_bot_ask_ryan']['ask_ryan_sending_msg_div']['ask_ryan_you-send-message'] = [
      '#type' => 'textfield',
      '#placeholder' => 'Chat',
      '#required' => true,
      '#attributes' => [
        'hidden' => false,
        'style' => '
        padding: 0px;
        text-align: center;
        width: 130%;
        flex: 2 1;
        margin: 0px 10px;
        outline: none;
        height: 30px;
        border-radius: 5px;',
        'class' => [''],
      ],
    ];
        $form['ai_support_bot_ask_ryan']['ask_ryan_sending_msg_div']['actions'] = [
      '#type' => 'button',
      '#value' => 'Send',
      '#ajax' => [
        'callback' => [$this, 'askRyanValidateSearchAjax'],
        'fade' => true,
        'event' => 'click',
        'method' => 'replace',
        'effect' => 'fade',
        'disable-refocus' => true,
        'progress' => [
          'type' => 'throbber',
        // 'message' => '',
        ],
      ],
      '#attributes' => [
        'class' => ['button', 'use-ajax'],
        'hidden' => true,
        'style' => '
        display: inline-block;
        position: relative;
        padding: 0px;
        min-width:40px;
        flex: 1 1;
        text-align: center;
        width: 100%;
        height: 32px;
        border-radius:0px;
        border:1px solid gray;
        float:right;
        margin: 13px 0px;
      cursor: pointer;
      color: black;
      background-color: white;
      transform: translateX(2px); ',
      ],
    ];

        $form['#cache'] = ['max-age' => 0];
        $form['#actions']['#submit'] = [$this, 'askRyanValidateSearchAjax'];
        $form['#attached']['library'][] = 'ai_support_bot/ai_support_js';

        return $form;
    }

    /**
     * Ajax callback to validate the Search field.
     */
    public function AskRyanAISupportBotValidator($form, $form_state)
    {
        $SearchCheck = $form_state->getValue('ask_ryan_you-send-message');
        if ($SearchCheck) {
            // Pass a key => value to other submit callbacks.
            if (str_replace(' ', '', $SearchCheck) != '') {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     *
     */
    public function beforeaskRyanValidateSearchAjax(array &$form, FormStateInterface $form_state)
    {
        // Remove the question from the field.
        $response = new AjaxResponse();
        $selectorSendReset = '#edit-ask-ryan-you-send-message';
        $Selector = '#edit-ask-ryan-answer-div';
        $response->addCommand(new InvokeCommand($selectorSendReset, 'val', ['']));
        $SearchCheck = $form_state->getValue('ask_ryan_you-send-message');
        $YourQuestionSearchCheck = t('<br><b style="color:blue;">You</b> ' . $SearchCheck);
        $response->addCommand(new AppendCommand($Selector, $YourQuestionSearchCheck));
        return $response;
    }

    /**
     *
     */
    public function askRyanValidateSearchAjax(array &$form, FormStateInterface $form_state)
    {
        $SearchCheck = $form_state->getValue('ask_ryan_you-send-message');
        $mute = $form_state->getValue('ask_ryan_mute_text_to_speech');
        // $oldMessage = $this->removePrefix($form, $form_state);
        $YourQuestionSearchCheck = '<div class="client" style="border:1px solid black;background:#f1f2f6;color:black;padding:10px;margin:10px auto;position:relative;item-align: right;width:80%"><b style="color:blue;">You</b><br>' . $SearchCheck . '</div>';
        $valid = $this->AskRyanAISupportBotValidator($form, $form_state);
        // $css = ['border' => '1px solid green'];
        $message = t($this->bot . ' Sure I can help you. let me check...');
        $response = new AjaxResponse();
        $selectorSendReset = '#edit-ask-ryan-you-send-message';
        $Selector = '#edit-ask-ryan-answer-div';
        $aiQuestions = new AIQAnswers();
        $response->addCommand(new InvokeCommand($selectorSendReset, 'val', ['']));
        $response->addCommand(new InvokeCommand('#edit-ask-ryan-answer-div', 'ajaxSuccess'));
        $answer = $aiQuestions->getQuestionAnswer($SearchCheck);
        if ($answer != 'no-answer') {
            // $Content = t('<br>' . $this->bot . ' ' . $answer);
            $response->addCommand(new AppendCommand($Selector, $YourQuestionSearchCheck));
            if ($this->mainConfig->get('text_to_speech') == 1 && !$mute) {
                $t2s = new AItextToSpeech();
                $audio = $t2s->text2Speech($answer);
            } else {
                $audio = '';
            }
            $Content = '<div class="ryan-answer" style="border:1px solid black;background:#f4f4f4;color:black;padding:10px;width:70%;"><b>' . $this->bot . '</b><br>' . $answer . $audio . '</div>';
            $response->addCommand(new AppendCommand($Selector, $Content));
            return $response;
        }
        // $css = ['border' => '1px solid red'];
        $message = t($this->bot . ' Sorry couldn\'t understand your question "' . $SearchCheck . '", can you be more specific please!');
        $response->addCommand(new AppendCommand($Selector, $YourQuestionSearchCheck));
        $response->addCommand(new AppendCommand($Selector, $message));
        return $response;
    }

    /**
     *
     */
    public function askRyanValidateSearchAjaxBasics($SearchCheck)
    {
        $standardQuestions = [
      "hi" => "Hello, My name is '. $this->bot . ' how can I help you today?",
      "bye" => "It was nice talking to you. Have a good one!",
      "no" => "Alright, I'm sorry, maybe you can ask me something else, or rephrase your question please",
    ];
        $Selector = '#search-valid-message';
        // $css = ['border' => '1px solid green'];
        foreach ($standardQuestions as $key => $value) {
            $Content = '<br>' . $this->bot . ' ' . $value;
        }
        return $Content;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        parent::validateForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        // Display result.
    }

    // /**
    //  *
    //  */
    // public function removePrefix(array &$form, FormStateInterface $form_state)
    // {
    //     $oldM = $form['ai_support_bot']['ask_ryan_answer_div']['#prefix'];
    //     if ($oldM != '') {
    //         $form['ai_support_bot']['ask_ryan_answer_div']['#prefix'] = '';
    //         return $oldM;
    //     } else {
    //         return $oldM;
    //     }
    // }
}
