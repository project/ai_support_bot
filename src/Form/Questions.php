<?php

namespace Drupal\ai_support_bot\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure settings for this site.
 */
class Settings extends ConfigFormBase {
  protected $connection;
  protected $config;

  /**
   * Constructs a new HelloForm object.
   */
  public function __construct() {
    $this->config = \Drupal::config('ai_support_bot.settings');
    $this->connection = \Drupal::database();
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ai_support_bot_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ai_support_bot.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $no_js_use = FALSE) {
    $form['questions_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => 'Questions and Answers',
      // Set up the wrapper so that AJAX will be able to replace the fieldset.
      '#description' => t('Questions better to be one word or max of 2, so the AI Bot would solve the question faster.'),
      '#prefix' => '<div id="questions-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    // Build the fieldset with the proper number of names. We'll use
    // $form_state['number_questions'] to determine the number of textfields to build.
    if ($this->config->get('number_questions') <= 0) {
      $form_state->setValue('number_questions', 1);
      $this->config->set('number_questions', 1);
    }
    $form['questions_fieldset']['number_questions'] = [
      '#type' => 'number',
      '#description' => t('Add/Remove Question by increasing the Questions number or decrease it to remove. <br>Important: Remove question would be from the bottom to top order. <br> ex. 4->3 questions means remove Question 4.'),
      '#attributes' => ['hidden' => FALSE],
      '#default_value' => $this->config->get('number_questions'),
    ];

    for ($i = 1; $i <= $this->config->get('number_questions'); $i++) {
      $form['questions_fieldset'][$i] = [
        '#type' => 'fieldset',
        '#title' => $i,
        '#required' => TRUE,
      ];
      $form['questions_fieldset'][$i]['question-' . $i] = [
        '#type' => 'textfield',
        '#id' => 'edit-question-' . $i,
        '#title' => t('Question ' . $i),
        '#required' => TRUE,
        '#attributes' => [
          'data-drupal-selector' => 'edit-question-' . $i,
        ],
        '#default_value' => $this->getQuestion($i) ? $this->getQuestion($i) : '',
      ];
      $form['questions_fieldset'][$i]['answer-' . $i] = [
        '#type' => 'textfield',
        '#id' => 'edit-answer-' . $i,
        '#title' => t('Answer ' . $i),
        '#required' => TRUE,
        '#attributes' => [
          'data-drupal-selector' => 'edit-answer-' . $i,
        ],
        '#default_value' => $this->getQuestion($i, 'answer') ? $this->getQuestion($i, 'answer') : '',
      ];
    }

    // @todo enable this feature of adding on spot using ajax
    // $form['add_question'] = array(
    //   '#type' => 'submit',
    //   '#value' => 'Add one more',
    //   '#submit' => array(
    //     'ajax_example_add_more_add_one',
    //   ),
    //   // properties of #ajax.
    //   '#ajax' => array(
    //     'callback' => [$this, 'addFormFields'],
    //     'wrapper' => 'questions-fieldset-wrapper',
    //   ),
    // );
    // if ($this->config->get('number_questions') > 0) {
    //     $form['remove_question'] = array(
    //     '#type' => 'submit',
    //     '#value' => 'Remove one',
    //     '#submit' => array(
    //       'questions_remove_callback',
    //     ),
    //     '#ajax' => array(
    //       'callback' => [$this, 'removeFormFields'],
    //       'wrapper' => 'questions-fieldset-wrapper',
    //     ),
    //   );
    // }
    // This simply allows us to demonstrate no-javascript use without
    // actually turning off javascript in the browser. Removing the #ajax
    // element turns off AJAX behaviors on that element and as a result
    // ajax.js doesn't get loaded.
    // For demonstration only! You don't need this.
    if ($no_js_use) {

      // Remove the #ajax from the above, so ajax.js won't be loaded.
      if (!empty($form['questions_fieldset']['remove_question']['#ajax'])) {
        unset($form['questions_fieldset']['remove_question']['#ajax']);
      }
      unset($form['questions_fieldset']['add_question']['#ajax']);
    }
    $form_state->setRebuild(TRUE);
    return parent::buildForm($form, $form_state);
  }

  /**
   *
   */
  public function addFormFields(array &$form, FormStateInterface $form_state) {
    $number = $form['questions_fieldset']['number_questions']['#value'] + 1;
    for ($i = 1; $i <= $number; $i++) {
      $form['questions_fieldset'][$i] = [
        '#type' => 'fieldset',
        '#title' => $i,
        '#required' => TRUE,
      ];
      $form['questions_fieldset'][$i]['question-' . $i] = [
        '#type' => 'textfield',
        '#id' => 'edit-question-' . $i,
        '#title' => t('Question ' . $i),
        '#required' => TRUE,
        '#attributes' => [
          'data-drupal-selector' => 'edit-question-' . $i,
        ],
        '#default_value' => $this->getQuestion($i) ? $this->getQuestion($i) : '',
      ];
      $form['questions_fieldset'][$i]['answer-' . $i] = [
        '#type' => 'textfield',
        '#id' => 'edit-answer-' . $i,
        '#title' => t('Answer ' . $i),
        '#required' => TRUE,
        '#attributes' => [
          'data-drupal-selector' => 'edit-answer-' . $i,
        ],
        '#default_value' => $this->getQuestion($i, 'answer') ? $this->getQuestion($i, 'answer') : '',
      ];
    }
    $form['questions_fieldset']['number_questions']['#value'] = $number;
    $form_state->setRebuild(TRUE);
    return $form['questions_fieldset'];
  }

  /**
   *
   */
  public function removeFormFields(array &$form, FormStateInterface $form_state) {
    $number = $form['questions_fieldset']['number_questions']['#value'] - 1;

    for ($i = 1; $i <= $number; $i++) {
      $form['questions_fieldset'][$i] = [
        '#type' => 'fieldset',
        '#title' => $i,
        '#required' => TRUE,
      ];
      $form['questions_fieldset'][$i]['question-' . $i] = [
        '#type' => 'textfield',
        '#id' => 'edit-question-' . $i,
        '#title' => t('Question ' . $i),
        '#required' => TRUE,
        '#attributes' => [
          'data-drupal-selector' => 'edit-question-' . $i,
        ],
        '#default_value' => $this->getQuestion($i) ? $this->getQuestion($i) : '',
      ];
      $form['questions_fieldset'][$i]['answer-' . $i] = [
        '#type' => 'textfield',
        '#id' => 'edit-answer-' . $i,
        '#title' => t('Answer ' . $i),
        '#required' => TRUE,
        '#attributes' => [
          'data-drupal-selector' => 'edit-answer-' . $i,
        ],
        '#default_value' => $this->getQuestion($i, 'answer') ? $this->getQuestion($i, 'answer') : '',
      ];
    }
    $form['questions_fieldset']['number_questions']['#value'] = $number;
    $form_state->setRebuild(TRUE);
    return $form['questions_fieldset'];
  }

  /**
   *
   *
   *
   */
  public function questions_add_more_callback(array &$form, FormStateInterface $form_state) {
    $this->addFormFields($form, $form_state);
    // $form_state->setRebuild(TRUE);
    // // generate captcha if not there
    // $number = $form_state->getValue('number_questions');
    // $response = new AjaxResponse();
    // $selector = '#edit-num-questions';
    // $method = 'val';
    // $arguments = [$number + 1];
    // $response->addCommand(new InvokeCommand($selector, $method, $arguments));
    // return $response;
  }

  /**
   *
   *
   *
   */
  public function questions_remove_callback(array &$form, FormStateInterface $form_state) {
    $this->removeFormFields($form, $form_state);
    // $form_state->setRebuild(TRUE);
    // // generate captcha if not there
    // $number = $form_state->getValue('number_questions');
    // $response = new AjaxResponse();
    // $selector = '#edit-num-questions';
    // $method = 'val';
    // $arguments = [$number - 1];
    // $response->addCommand(new InvokeCommand($selector, $method, $arguments));
    // return $response;
  }

  /**
   * Get question.
   */
  public function getQuestion($i = 0, $answer = FALSE) {
    $answers = '';
    $questions = '';
    $result = [];
    $query = db_select('ai_support_bot', 'v');
    $query->fields('v');
    $results = $query->execute()->fetchAll();
    foreach ($results as $key => $value) {
      if ($key == $i - 1) {
        if ($answer) {
          return (isset($value->answers)) ? $value->answers : '';
        }
        return (isset($value->words)) ? $value->words : '';
      }
    }
  }

  /**
   * Store question.
   */
  private function storeQuestion(array &$form, FormStateInterface $form_state) {
    global $user;
    $number = $form['questions_fieldset']['number_questions']['#value'];
    // Empty the table.
    \Drupal::database()->truncate('ai_support_bot')->execute();
    $values = [];
    for ($i = 1; $i <= $number; $i++) {
      if (isset($form['questions_fieldset'][$i]['question-' . $i]['#value'])) {
        $values[$i] = [
          'words' => $form['questions_fieldset'][$i]['question-' . $i]['#value'],
          'answers' => $form['questions_fieldset'][$i]['answer-' . $i]['#value'],
          'qid' => $i,
          'id' => $i,
          'created' => \Drupal::time()->getRequestTime(),
          'status' => 1,
        ];
      }
    }
    $query = $this->connection->insert('ai_support_bot')->fields(['answers', 'words', 'qid', 'id', 'created', 'status']);
    foreach ($values as $record) {
      $query->values($record);
    }
    $query->execute();
    return;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable('ai_support_bot.settings')
      ->set('number_questions', $form_state->getValue('number_questions'))
      ->save();
    $this->storeQuestion($form, $form_state);
    $this->storeQuestion($form, $form_state);
    parent::submitForm($form, $form_state);
  }

}
