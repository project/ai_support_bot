<?php

namespace Drupal\ai_support_bot\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure settings for this site.
 */
class Settings extends ConfigFormBase
{
    protected $connection;

    /**
     * Constructs a new HelloForm object.
     */
    public function __construct()
    {
        $this->connection = \Drupal::database();
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'ai_support_bot_settings';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames()
    {
        return [
      'ai_support_bot.settings',
    ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $no_js_use = false)
    {
        $config = $this->config('ai_support_bot.settings');
        $query = db_select('ai_support_bot', 'v');
        $query->fields('v');
        $results = $query->countQuery()->execute()->fetchField();
        $form['questions_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => 'Questions and Answers',
          // Set up the wrapper so that AJAX will be able to replace the fieldset.
      '#description' => t('Questions better to be one word or max of 2, so the AI Bot would solve the question faster.'),
      '#prefix' => '<div id="questions-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

        // Build the fieldset with the proper number of names. We'll use
        // $form_state['number_questions'] to determine the number of textfields to build.
        if ($config->get('number_questions') <= 0 || $results <= 0) {
            $form_state->setValue('number_questions', 1);
            $config->set('number_questions', 1);
        } elseif ($results > 0 && $results > $config->get('number_questions')) {
            $form_state->setValue('number_questions', $results);
            $config->set('number_questions', $results);
        }
        $form['questions_fieldset']['number_questions'] = [
      '#type' => 'number',
      '#description' => t('Add/Remove Question by increasing the Questions number or decrease it to remove. <br>Important: Remove question would be from the bottom to top order. <br> ex. 4->3 questions means remove Question 4.'),
      '#attributes' => ['hidden' => false],
      '#default_value' => $config->get('number_questions'),
    ];
        $form['questions_fieldset']['startup_support_message'] = [
      '#type' => 'textarea',
      '#title' => 'Startup Message',
      '#description' => t('Startup message is when the form loads, what would you like the first support message to be. ex. <br> Hello, my name is Ryan, I\m here to help you. '),
      '#attributes' => ['hidden' => false],
      '#default_value' => ($config->get('startup_support_message')) ?: 'Hello, my name is Ryan, and I\'m an AI Robot support here to help you. If you have any question. Please do not hesitate to ask me as you are asking real human.',
    ];
        $form['questions_fieldset']['text_to_speech'] = [
      '#type' => 'checkbox',
      '#title' => 'Text to Speech powered by Google Translator',
      '#description' => t('Make your AI Support more interactive, by Using Text to Speech.'),
      '#attributes' => ['hidden' => false],
      '#default_value' => $config->get('text_to_speech'),
    ];
        $form['questions_fieldset']['share_analytic_ddkits'] = [
      '#type' => 'checkbox',
      '#title' => 'Analytics',
      '#description' => t('Share analytics would let you see more about what your Ryan AI is answering, and store all questions for review.'),
      '#attributes' => ['hidden' => false],
      '#default_value' => $config->get('share_analytic_ddkits'),
    ];
        $form['questions_fieldset']['share_analytic_ddkits_token'] = [
          '#type' => 'fieldset',
          '#title' => 'DDKits Analytics',
          '#states' => [
              // Only show this field when the 'toggle_me' checkbox is enabled.
            'visible' => [
              ':input[name="share_analytic_ddkits"]' => [
                'checked' => true,
              ],
            ],
          ],
          // Set up the wrapper so that AJAX will be able to replace the fieldset.
          '#description' => t('To get new token:
                <br> 1- Go to <a target="_blank" href="https://ryan.ddkits.com/">Ryan AI</a>
                <br> 2- Create new User
                <br> 3- copy your username and password in here.
                <br> after these steps you can check your AI support Status and analytics on https://ryan.ddkits.com '),
        ];
        $form['questions_fieldset']['share_analytic_ddkits_token']['share_analytic_ddkits_username'] = [
          '#type' => 'textfield',
          '#title' => 'Username',
          '#attributes' => ['hidden' => false],
          '#default_value' => $config->get('share_analytic_ddkits_username'),
        ];

        $form['questions_fieldset']['share_analytic_ddkits_token']['share_analytic_ddkits_password'] = [
          '#type' => 'password',
          '#title' => 'Password',
          '#attributes' => ['hidden' => false],
          '#default_value' => $config->get('share_analytic_ddkits_password'),
        ];

        for ($i = 1; $i <= $config->get('number_questions'); $i++) {
            $form['questions_fieldset'][$i] = [
        '#type' => 'fieldset',
        '#title' => $i,
        '#required' => true,
      ];
            $form['questions_fieldset'][$i]['question-' . $i] = [
        '#type' => 'textfield',
        '#id' => 'edit-question-' . $i,
        '#title' => t('Question ' . $i),
        '#description' => 'To add more than one keyword for this question, <br>use <b><or></b> between the keywords. ex. hi<b><or></b>hello<b><or></b>hey, all these questions would be answered similar.',
        '#required' => true,
        '#attributes' => [
          'data-drupal-selector' => 'edit-question-' . $i,
        ],
        '#default_value' => $this->getQuestion($i) ? $this->getQuestion($i) : '',
      ];
            $form['questions_fieldset'][$i]['answer-' . $i] = [
        '#type' => 'textfield',
        '#id' => 'edit-answer-' . $i,
        '#title' => t('Answer ' . $i),
        '#required' => true,
        '#attributes' => [
          'data-drupal-selector' => 'edit-answer-' . $i,
        ],
        '#default_value' => $this->getQuestion($i, 'answer') ? $this->getQuestion($i, 'answer') : '',
      ];
        }

        // @todo enable this feature of adding on spot using ajax
        // $form['add_question'] = array(
        //   '#type' => 'submit',
        //   '#value' => 'Add one more',
        //   '#submit' => array(
        //     'ajax_example_add_more_add_one',
        //   ),
        //   // properties of #ajax.
        //   '#ajax' => array(
        //     'callback' => [$this, 'addFormFields'],
        //     'wrapper' => 'questions-fieldset-wrapper',
        //   ),
        // );
        // if ($config->get('number_questions') > 0) {
        //     $form['remove_question'] = array(
        //     '#type' => 'submit',
        //     '#value' => 'Remove one',
        //     '#submit' => array(
        //       'questions_remove_callback',
        //     ),
        //     '#ajax' => array(
        //       'callback' => [$this, 'removeFormFields'],
        //       'wrapper' => 'questions-fieldset-wrapper',
        //     ),
        //   );
        // }
        // This simply allows us to demonstrate no-javascript use without
        // actually turning off javascript in the browser. Removing the #ajax
        // element turns off AJAX behaviors on that element and as a result
        // ajax.js doesn't get loaded.
        // For demonstration only! You don't need this.
        if ($no_js_use) {

      // Remove the #ajax from the above, so ajax.js won't be loaded.
            if (!empty($form['questions_fieldset']['remove_question']['#ajax'])) {
                unset($form['questions_fieldset']['remove_question']['#ajax']);
            }
            unset($form['questions_fieldset']['add_question']['#ajax']);
        }
        $form_state->setRebuild(true);
        return parent::buildForm($form, $form_state);
    }

    /**
     *
     */
    public function addFormFields(array &$form, FormStateInterface $form_state)
    {
        $number = $form['questions_fieldset']['number_questions']['#value'] + 1;
        for ($i = 1; $i <= $number; $i++) {
            $form['questions_fieldset'][$i] = [
        '#type' => 'fieldset',
        '#title' => $i,
        '#required' => true,
      ];
            $form['questions_fieldset'][$i]['question-' . $i] = [
        '#type' => 'textfield',
        '#id' => 'edit-question-' . $i,
        '#title' => t('Question ' . $i),
        '#required' => true,
        '#attributes' => [
          'data-drupal-selector' => 'edit-question-' . $i,
        ],
        '#default_value' => $this->getQuestion($i) ? $this->getQuestion($i) : '',
      ];
            $form['questions_fieldset'][$i]['answer-' . $i] = [
        '#type' => 'textarea',
        '#id' => 'edit-answer-' . $i,
        '#title' => t('Answer ' . $i),
        '#required' => true,
        '#attributes' => [
          'data-drupal-selector' => 'edit-answer-' . $i,
        ],
        '#default_value' => $this->getQuestion($i, 'answer') ? $this->getQuestion($i, 'answer') : '',
      ];
        }
        $form['questions_fieldset']['number_questions']['#value'] = $number;
        $form_state->setRebuild(true);
        return $form['questions_fieldset'];
    }

    /**
     *
     */
    public function removeFormFields(array &$form, FormStateInterface $form_state)
    {
        $number = $form['questions_fieldset']['number_questions']['#value'] - 1;

        for ($i = 1; $i <= $number; $i++) {
            $form['questions_fieldset'][$i] = [
        '#type' => 'fieldset',
        '#title' => $i,
        '#required' => true,
      ];
            $form['questions_fieldset'][$i]['question-' . $i] = [
        '#type' => 'textfield',
        '#id' => 'edit-question-' . $i,
        '#title' => t('Question ' . $i),
        '#required' => true,
        '#attributes' => [
          'data-drupal-selector' => 'edit-question-' . $i,
        ],
        '#default_value' => $this->getQuestion($i) ? $this->getQuestion($i) : '',
      ];
            $form['questions_fieldset'][$i]['answer-' . $i] = [
        '#type' => 'textfield',
        '#id' => 'edit-answer-' . $i,
        '#title' => t('Answer ' . $i),
        '#required' => true,
        '#attributes' => [
          'data-drupal-selector' => 'edit-answer-' . $i,
        ],
        '#default_value' => $this->getQuestion($i, 'answer') ? $this->getQuestion($i, 'answer') : '',
      ];
        }
        $form['questions_fieldset']['number_questions']['#value'] = $number;
        $form_state->setRebuild(true);
        return $form['questions_fieldset'];
    }

    /**
     *
     *
     *
     */
    public function questions_add_more_callback(array &$form, FormStateInterface $form_state)
    {
        $this->addFormFields($form, $form_state);
        // $form_state->setRebuild(TRUE);
    // // generate captcha if not there
    // $number = $form_state->getValue('number_questions');
    // $response = new AjaxResponse();
    // $selector = '#edit-num-questions';
    // $method = 'val';
    // $arguments = [$number + 1];
    // $response->addCommand(new InvokeCommand($selector, $method, $arguments));
    // return $response;
    }

    /**
     *
     *
     *
     */
    public function questions_remove_callback(array &$form, FormStateInterface $form_state)
    {
        $this->removeFormFields($form, $form_state);
        // $form_state->setRebuild(TRUE);
    // // generate captcha if not there
    // $number = $form_state->getValue('number_questions');
    // $response = new AjaxResponse();
    // $selector = '#edit-num-questions';
    // $method = 'val';
    // $arguments = [$number - 1];
    // $response->addCommand(new InvokeCommand($selector, $method, $arguments));
    // return $response;
    }

    /**
     * Get question.
     */
    public function getQuestion($i = 0, $answer = false)
    {
        $answers = '';
        $questions = '';
        $result = [];

        $query = db_select('ai_support_bot', 'v');
        $query->fields('v');
        $results = $query->execute()->fetchAll();
        foreach ($results as $key => $value) {
            if ($key == $i - 1) {
                if ($answer) {
                    return (isset($value->answers)) ? $value->answers : '';
                }
                return (isset($value->words)) ? $value->words : '';
            }
        }
    }

    /**
     * Store question.
     */
    private function storeQuestion(array &$form, FormStateInterface $form_state)
    {
        global $user;
        $number = $form['questions_fieldset']['number_questions']['#value'];
        // Empty the table.
        \Drupal::database()->truncate('ai_support_bot')->execute();
        $values = [];
        for ($i = 1; $i <= $number; $i++) {
            if (isset($form['questions_fieldset'][$i]['question-' . $i]['#value'])) {
                $values[$i] = [
          'words' => $form['questions_fieldset'][$i]['question-' . $i]['#value'],
          'answers' => $form['questions_fieldset'][$i]['answer-' . $i]['#value'],
          'qid' => $i,
          'id' => $i,
          'created' => \Drupal::time()->getRequestTime(),
          'status' => 1,
        ];
            }
        }
        $query = $this->connection->insert('ai_support_bot')->fields(['answers', 'words', 'qid', 'id', 'created', 'status']);
        foreach ($values as $record) {
            $query->values($record);
        }
        $query->execute();
        return;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $saveQ= $this->configFactory->getEditable('ai_support_bot.settings')
          ->set('startup_support_message', $form_state->getValue('startup_support_message'))
          ->set('number_questions', $form_state->getValue('number_questions'))
          ->set('text_to_speech', $form_state->getValue('text_to_speech'))
          ->set('share_analytic_ddkits', $form_state->getValue('share_analytic_ddkits'))
          ->set('share_analytic_ddkits_username', $form_state->getValue('share_analytic_ddkits_username'));
        if ($form_state->getValue('share_analytic_ddkits_password') != '') {
            $saveQ->set('share_analytic_ddkits_password', $form_state->getValue('share_analytic_ddkits_password'));
        }
        $saveQ->save();
        $this->storeQuestion($form, $form_state);
        parent::submitForm($form, $form_state);
    }
}
