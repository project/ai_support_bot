<?php

namespace Drupal\ai_support_bot\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\ai_support_bot\Controller\AIQAnswers;
use Drupal\ai_support_bot\Controller\AItextToSpeech;

/**
 *
 */
class AIForm extends FormBase
{
    protected $connection;
    protected $mainConfig;
    protected $newChat;
    protected $bot;

    /**
     * Constructs a new object.
     */
    public function __construct()
    {
        $this->bot = '<b>Ryan </b>';
        $this->mainConfig = \Drupal::config('ai_support_bot.settings');
        $this->connection = \Drupal::database();
        $this->newChat = true;
    }

    /**
     * Returns a unique string identifying the form.
     *
     * The returned ID should be a unique string that can be a valid PHP function
     * name, since it's used in hook implementation names such as
     * hook_form_FORM_ID_alter().
     *
     * @return string
     *   The unique string identifying the form.
     */
    public function getFormId()
    {
        return 'ai_support_bot_form';
    }

    /**
     * Form constructor.
     *
     * @param array $form
     *   An associative array containing the structure of the form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The current state of the form.
     *
     * @return array
     *   The form structure.
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['ai_support_bot']['search_div'] = [
      '#type' => 'fieldset',
      '#weight' => 0,
      '#attributes' => [
        'hidden' => false,
        'style' => 'display:block;position:relative;',
        'class' => ['form-control'],
      ],
    ];
        $form['ryan_ask_startup_message'] = [
      '#type' => 'hidden',
      '#value' => '<div class="ryan-questions" style="border:1px solid black;background:#f4f4f4;color:black;padding:10px;width:70%;"><b>' . $this->bot . '</b><br>' . $this->mainConfig->get('startup_support_message') . '</div>',
    ];
        $form['ai_support_bot']['search_div']['answer_div'] = [
      '#type' => 'container',
      '#weight' => 0,
      '#attributes' => [
        'hidden' => false,
        'style' => 'display:inline-block;overflow: auto;overflow-y: scroll;position:relative;width:100%;max-height:300px;min-height:50px;margin: 0px;',
        'class' => ['search-valid-message-answers'],
      ],
    ];
        // Text to speech.
        if ($this->mainConfig->get('text_to_speech') == 1) {
            $form['ai_support_bot']['search_div']['mute_text_to_speech'] = [
        '#type' => 'checkbox',
        '#title' => 'Mute',
        '#default_value' => true,
      // '#description' => t('Check this box to Mute me if you don\'t like to hear my voice.'),
      ];
        }

        $form['ai_support_bot']['search_div']['you-send-message'] = [
      '#type' => 'textfield',
      '#title' => 'Chat',
      '#required' => true,
      '#attributes' => [
        'hidden' => false,
        'required' => true,
        'style' => 'margin: 0px;
        position: relative;
        padding: 0px;
        text-align: center;
        width: 100%;
        outline: none;
        height: 30px;
        border-radius: 5px;',
        'class' => ['form-control'],
      ],
    ];
        $form['ai_support_bot']['search_div']['actions'] = [
      '#type' => 'button',
      '#value' => 'Send',
      '#ajax' => [
        'callback' => [$this, 'validateSearchAjax'],
        'fade' => true,
        'event' => 'click',
        'method' => 'replace',
        'effect' => 'fade',
        'disable-refocus' => true,
        'progress' => [
          'type' => 'throbber',
          'message' => $this->bot . 'is writing',
        ],
      ],
      '#attributes' => [
        'class' => ['button', 'use-ajax'],
        'hidden' => true,
        'style' => '
      position: relative;
      height: 30px;
      padding:0px;
      margin: 5px 0 10px 0;
      width: 100%;
      border-radius: 5px;
      border: none;
      cursor: pointer;
      color: black;
      background-color: gray;
      transform: translateX(2px); ',
      ],
    ];

        $form['#cache'] = ['max-age' => 0];
        $form['#actions']['#submit'] = [$this, 'validateSearchAjax'];
        $form['#attached']['library'][] = 'ai_support_bot/ai_support_js';

        return $form;
    }

    /**
     * Ajax callback to validate the Search field.
     */
    public function AISupportBotValidator($form, $form_state)
    {
        if ($form_state->hasValue('you-send-message')) {
            // Pass a key => value to other submit callbacks.
            $SearchCheck = $form_state->getValue('you-send-message');
            if (str_replace(' ', '', $SearchCheck) != '') {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     *
     */
    public function beforeValidateSearchAjax(array &$form, FormStateInterface $form_state)
    {
        // Remove the question from the field.
        $response = new AjaxResponse();
        $selectorSendReset = '#edit-you-send-message';
        $Selector = '#edit-answer-div';
        $response->addCommand(new InvokeCommand($selectorSendReset, 'val', ['']));
        $SearchCheck = $form_state->getValue('you-send-message');
        $YourQuestionSearchCheck = t('<br><b style="color:blue;">You</b> ' . $SearchCheck);
        $response->addCommand(new AppendCommand($Selector, $YourQuestionSearchCheck));
        return $response;
    }

    /**
     *
     */
    public function validateSearchAjax(array &$form, FormStateInterface $form_state)
    {
        $SearchCheck = $form_state->getValue('you-send-message');
        $mute = $form_state->getValue('mute_text_to_speech');
        // $oldMessage = $this->removePrefix($form, $form_state);
        $YourQuestionSearchCheck = '<div class="client" style="border:1px solid black;background:#f1f2f6;color:black;padding:10px;margin:10px auto;position:relative;item-align: right;width:80%"><b style="color:blue;">You</b><br>' . $SearchCheck . '</div>';
        $valid = $this->AISupportBotValidator($form, $form_state);
        // $css = ['border' => '1px solid green'];
        $message = t($this->bot . ' Sure I can help you. let me check...');
        $response = new AjaxResponse();
        $selectorSendReset = '#edit-you-send-message';
        $Selector = '#edit-answer-div';
        $aiQuestions = new AIQAnswers();
        $response->addCommand(new InvokeCommand($selectorSendReset, 'val', ['']));
        $response->addCommand(new InvokeCommand('#edit-answer-div', 'ajaxSuccess'));
        $answer = $aiQuestions->getQuestionAnswer($SearchCheck);
        if ($answer != 'no-answer') {
            // $Content = t('<br>' . $this->bot . ' ' . $answer);
            $response->addCommand(new AppendCommand($Selector, $YourQuestionSearchCheck));
            if ($this->mainConfig->get('text_to_speech') == 1 && !$mute) {
                $t2s = new AItextToSpeech();
                $audio = $t2s->text2Speech($answer);
            } else {
                $audio = '';
            }
            $Content = '<div class="ryan-answer" style="border:1px solid black;background:#f4f4f4;color:black;padding:10px;width:70%;"><b>' . $this->bot . '</b><br>' . $answer . $audio . '</div>';
            $response->addCommand(new AppendCommand($Selector, $Content));
            return $response;
        }
        // $css = ['border' => '1px solid red'];
        $message = t($this->bot . ' Sorry couldn\'t understand your question "' . $SearchCheck . '", can you be more specific please!');
        $response->addCommand(new AppendCommand($Selector, $YourQuestionSearchCheck));
        $response->addCommand(new AppendCommand($Selector, $message));
        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        parent::validateForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        // Display result.
    }

    // /**
    //  *
    //  */
    // public function removePrefix(array &$form, FormStateInterface $form_state)
    // {
    //     $oldM = $form['ai_support_bot']['search_div']['answer_div']['#prefix'];
    //     if ($oldM != '') {
    //         $form['ai_support_bot']['search_div']['answer_div']['#prefix'] = '';
    //         return $oldM;
    //     } else {
    //         return $oldM;
    //     }
    // }
}
