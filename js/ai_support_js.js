(function ($, Drupal) {

  Drupal.behaviors.ai_support_behavior = {
    attach: function(context, settings) {

      $(document).ajaxStart(function () {
        $('#edit-you-send-message').attr("disabled","disabled");
        $('#edit-actions').attr("disabled","disabled");
        $('#edit-ask-ryan-you-send-message').attr("disabled","disabled");
        $('#edit-ask-ryan-actions').attr("disabled","disabled");
      });
      $(document).ajaxSuccess(function () {
        //once a second
        var elementRyan = document.getElementById("edit-ask-ryan-answer-div");
        var element = document.getElementById("edit-answer-div");
        $('#edit-you-send-message').removeAttr("disabled");
        $('#edit-actions').removeAttr("disabled");
        $('#edit-ask-ryan-you-send-message').removeAttr("disabled");
        $('#edi-ask-ryan-actions').removeAttr("disabled");
        element.scrollTop = element.scrollHeight;
        elementRyan.scrollTop = elementRyan.scrollHeight;
      });

      // Ask ryan functions
      $(document).ready(function(){
        $("#edit-hide-ryan-ask").on("click", function(){
          $('#edit-ai-support-bot-ask-ryan').hide('slow');
          $("#edit-show-ryan-ask").show('slow');
        });
        $("#edit-show-ryan-ask").on("click", function(){
          $('#edit-ai-support-bot-ask-ryan').show('slow');
          $(this).hide();
        });

      });

      // end of attach
    }
  };
    // Add startup message
    $('#edit-ask-ryan-answer-div').ready(function() {
      var valueOfText = $('input[name=ryan_ask_startup_message]').attr('value');
      // var valueOfText = "<div style=\"color:black;background:white;padding:3px;\"><b>Ryan </b> Hi, I’m Ryan. I'm programmed to help you with any question. What would you like to ask me?</div>";
      // NOW ADD THE VALUE (RANDOM NUMBER) TO THE DIV ELEMENT.
      $('#edit-ask-ryan-answer-div').append(valueOfText);
    });
    $('#edit-answer-div').ready(function() {
      var valueOfText = $('input[name=ryan_ask_startup_message]').attr('value');
      // var valueOfText = "<div style=\"color:black;background:white;padding:3px;\"><b>Ryan </b> Hi, I’m Ryan. I'm programmed to help you with any question. What would you like to ask me?</div>";
      // NOW ADD THE VALUE (RANDOM NUMBER) TO THE DIV ELEMENT.
      $('#edit-answer-div').append(valueOfText);
    });
})(jQuery, Drupal);


